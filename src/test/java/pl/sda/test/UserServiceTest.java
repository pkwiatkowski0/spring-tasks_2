package pl.sda.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.beans.User;
import pl.sda.beans.service.UserService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by patry on 12.03.2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-config.xml"})
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void isUserServiceInjected() throws Exception {
        assertNotNull(userService);
    }

    @Test
    public void testGetAllUsers() throws Exception {
        assertNotNull(userService);
        assertNotNull(userService.getAllUsers());
        assertEquals("Patryk", userService.getAllUsers().get(0).getName());
        assertEquals("Kwiatkowski", userService.getAllUsers().get(0).getSurname());
        assertEquals(24, userService.getAllUsers().get(0).getAge());
        assertEquals("pkwiatkowski", userService.getAllUsers().get(0).getLogin());

        assertEquals("Janusz", userService.getAllUsers().get(1).getName());
        assertEquals("Kowalski", userService.getAllUsers().get(1).getSurname());
        assertEquals(63, userService.getAllUsers().get(1).getAge());
        assertEquals("jkowalski", userService.getAllUsers().get(1).getLogin());

        assertEquals("Grażyna", userService.getAllUsers().get(2).getName());
        assertEquals("Kowalska", userService.getAllUsers().get(2).getSurname());
        assertEquals(60, userService.getAllUsers().get(2).getAge());
        assertEquals("gkowalska", userService.getAllUsers().get(2).getLogin());
    }

    @Test
    public void testGetUserByLogin() throws Exception {
        assertNotNull(userService);
        assertEquals("Patryk", userService.getUserByLogin("pkwiatkowski").getName());
        assertEquals("Kowalski", userService.getUserByLogin("jkowalski").getSurname());
        assertEquals(60, userService.getUserByLogin("gkowalska").getAge());
    }

    @Test
    public void testAddUser() {
        assertNotNull(userService);
        userService.addUser(getNewUser("Mieczysław", "Krzyżanowski", 42, "mkrzyżanowski"));
        assertEquals("Mieczysław", userService.getAllUsers().get(3).getName());
        assertEquals("Krzyżanowski", userService.getAllUsers().get(3).getSurname());
        assertEquals(42, userService.getAllUsers().get(3).getAge());
        assertEquals("mkrzyżanowski", userService.getAllUsers().get(3).getLogin());

        assertEquals("Mieczysław", userService.getUserByLogin("mkrzyżanowski").getName());
        assertEquals("Krzyżanowski", userService.getUserByLogin("mkrzyżanowski").getSurname());
        assertEquals(42, userService.getUserByLogin("mkrzyżanowski").getAge());


    }

    private User getNewUser(String name, String surname, int age, String login) {
        User newUser = new User();
        newUser.setName(name);
        newUser.setSurname(surname);
        newUser.setAge(age);
        newUser.setLogin(login);
        return newUser;
    }

}
