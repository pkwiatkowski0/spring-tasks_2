package pl.sda.beans.service;

import org.springframework.beans.factory.annotation.Autowired;
import pl.sda.beans.DAO.UserDAO;
import pl.sda.beans.User;

import java.util.List;

/**
 * Created by patry on 12.03.2017.
 */
public class DefaultUserService implements UserService {
    @Autowired
    private UserDAO userDao;

    @Override
    public List<User> getAllUsers() {
       return userDao.getAllUsers();
    }

    @Override
    public User getUserByLogin(String login) {
        return userDao.getUserByLogin(login);
    }

    @Override
    public void addUser(User user) {
        userDao.addUser(user);

    }

}
