package pl.sda.beans.service;

import pl.sda.beans.User;

import java.util.List;

/**
 * Created by patry on 12.03.2017.
 */
public interface UserService {

     List<User> getAllUsers();
     User getUserByLogin(String login);
     void addUser(User user);
}
