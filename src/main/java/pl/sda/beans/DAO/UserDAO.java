package pl.sda.beans.DAO;

import pl.sda.beans.User;

import java.util.List;

/**
 * Created by patry on 12.03.2017.
 */
public interface UserDAO {
   List<User> getAllUsers();
   User getUserByLogin(String login);
   void addUser(User user);

}
