package pl.sda.beans.DAO;

import pl.sda.beans.User;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * Created by patry on 12.03.2017.
 */
public class InMemoryUserDAO implements UserDAO {
    List<User> usersList;

    public List<User> getAllUsers() {
        return usersList;
    }

    public User getUserByLogin(String login) {
        return usersList.stream().filter(o -> o.getLogin().equals(login)).findAny().get();
    }

    @Override
    public void addUser(User user) {
        usersList.add(user);
    }

    public List<User> getUsersList() {
        return usersList;
    }

    public void setUsersList(List<User> usersList) {
        this.usersList = usersList;
    }
}
